var gulp          = require('gulp'),
  less            = require('gulp-less'),
  sourcemaps      = require('gulp-sourcemaps'),
  watch           = require('gulp-watch'),
  browserSync     = require('browser-sync'),
  minifyCSS       = require('gulp-minify-css'),
  mainBowerFiles  = require('main-bower-files'),
  bowerFiles      = mainBowerFiles([['**/*.js']]),
  uglify          = require('gulp-uglify'),
  concat          = require('gulp-concat'),
  autoprefixer    = require('gulp-autoprefixer'),
  rename          = require('gulp-rename'),
  plumber         = require('gulp-plumber'),
  notify          = require('gulp-notify'),
  util            = require('gulp-util');

console.info('********** Bower Files **********');
console.info(bowerFiles);

/* ==========================================================
* Default task
* ===========================================================
*/
gulp.task('default', [
  'copyAssets',
  'browser-sync',
  'pluginsConcat',
  'pluginsCssConcat',
  'jsConcat',
  'copyHtml',
  'watch',
  'less'
]);

/* ==========================================================
* Build task
* ===========================================================
*/
gulp.task('build', [
  'copyAssets',
  'pluginsConcat',
  'pluginsCssConcat',
  'jsConcat',
  'less-min'
]);

/* ==========================================================
* Copy assets to public
* ===========================================================
*/
gulp.task('copyAssets', function() {
  'use strict';
  gulp.src([
      'assets/**/*.*',
      '!assets/less/*.*'
    ])
    .pipe(gulp.dest('public'));
});

/* ==========================================================
* Copy html to public
* ===========================================================
*/
gulp.task('copyHtml', function () {
  gulp.src('app/templates/*.html')
    .pipe(gulp.dest('./public'));
});

/* ==========================================================
* CSS plugins
* ===========================================================
*/
gulp.task('pluginsCssConcat', function() {
  gulp.src(mainBowerFiles([
      ['**/*.css']
    ]))
    .pipe(concat('plugins.css'))
    .pipe(gulp.dest('public/css'));
});

/* ==========================================================
* JS plugins
* ===========================================================
*/
gulp.task('pluginsConcat', function() {
  gulp.src(bowerFiles)
    .pipe(concat('plugins.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'));
});

/* ==========================================================
* JS concat
* ===========================================================
*/
gulp.task('jsConcat', function() {
  gulp.src(['app/js/**/*.js'])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .on('error', notify.onError(function(error) {
      return '\nAn error occurred while uglifying js.\nLook in the console for details.\n' + error;
    }))
    .pipe(sourcemaps.write('../js'))
    .pipe(gulp.dest('public/js'));
});

/* ==========================================================
* Browser sync
* ===========================================================
*/
gulp.task('browser-sync', function() {
  var files = [
    'public/**/*.html',
    'public/js/**/*.js',
    'public/css/**/*.css'
  ];

  browserSync.init(files, {
    server: {
      baseDir: './public'
    },
    open: false
  });
});

/* ==========================================================
* Watch
* ===========================================================
*/
gulp.task('watch', function() {
  gulp.watch('assets/less/*.less', ['less']);
  gulp.watch('app/js/**/*.js', ['jsConcat']);
  gulp.watch('app/templates/**/*.html', ['copyHtml']);
});

/* ==========================================================
* Less
* ===========================================================
*/
gulp.task('less', function() {
  gulp.src('assets/less/app.less')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(less({
      relativeUrls: true
    }))
    .on('error', notify.onError(function(error) {
      return '\nAn error occurred while compiling css.\nLook in the console for details.\n' + error;
    }))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('../css'))
    .pipe(gulp.dest('public/css'));
});

/* ==========================================================
* Less min
* ===========================================================
*/
gulp.task('less-min', function() {
  gulp.src('assets/less/app.less')
    .pipe(plumber())
    .pipe(less())
    .on('error', notify.onError(function(error) {
      return '\nAn error occurred while compiling css.\nLook in the console for details.\n' + error;
    }))
    .pipe(autoprefixer({
      browsers: ['last 5 versions'],
      cascade: false
    }))
    .pipe(minifyCSS({
      keepBreaks: false,
      keepSpecialComments: true,
      benchmark: false,
      debug: true
    }))
    .pipe(gulp.dest('public/css'));
});
