﻿$(document).ready(function(){
  // Logic of choosing your family
  $('#privacy, #policy, input:radio[name=family], input:radio[name=member]').change(function(){
    var $family = $('input:radio[name=family]');
    var $member = $('input:radio[name=member]');

    if ( ($('#privacy').is(':checked')) && ($('#policy').is(':checked')) && ( $family.is(':checked') === true ) && ( $member.is(':checked') === true ) ) {
      $('#submitStock').prop('disabled', false);
    }
    else {
      $('#submitStock').prop('disabled', true);
    }
  });

  $('input:radio[name=family]').change(function(){
    if ( $('#add').is(':checked') === true ) {
      $('#familyCode').addClass('open');
      setTimeout( 
        function(){
          $('#familyCodeInput').focus();
        }, 
      1000)
    }
    else if ( $('#new').is(':checked') === true ) {
      $('#familyCode').removeClass('open');
    }
  });

  // Mobile menu toggle
  $('#btnToggle').click(function(){
    $('body').toggleClass('mobile-menu-open');
  });
  $('#familyCodeInput').bind('keyup',function(){
    $(this).val($(this).val().replace(/[^a-z0-9 ]/i, ""))
  });

  $('#submitStock').click(function(){
    $('#preloader').addClass('preloader_open');
    setTimeout( 
      function(){
        $('#preloader').removeClass('preloader_open');
      }, 
    2000)
  });

});


/* new menu */
$(function() {
    //mainmenu
    $('.b-mainmenu__item, .b-mainmenu__item_btn.all_desktop, .b-mainmenu__item_btn.all_tablet').hover(
        function(){
            if (!Modernizr.touch)
                $(this).addClass('opened').find('.b-mainmenu_drop').stop().slideDown(300, function(){$(this).css({height: 'auto'})});
        },
        function(){
            if (!Modernizr.touch)
                $(this).removeClass('opened').find('.b-mainmenu_drop').stop().hide();
        }
    );

    // mainmenu level 1
    $('.b-mainmenu__item .b-mainmenu_link, .b-mainmenu__item_btn.all_desktop .b-mainmenu_link, .b-mainmenu__item_btn.all_tablet .b-mainmenu_link').click(function(e){
        if (Modernizr.touch) {
            var menuItem = $(this).parent();
            if (menuItem.hasClass('opened')) {
                menuItem.removeClass('opened').find('.b-mainmenu_drop').stop().slideUp(300);
            }
            else {
                $('.b-mainmenu_drop').stop().hide();
                $('.b-mainmenu__item').removeClass('opened');
                menuItem.addClass('opened').find('.b-mainmenu_drop').stop().slideDown(300,function(){$(this).css({height: 'auto'})});
            }
        }
    });
    
    $('.b-mainmenu_drop_slideup').click(function(e){
      $('.b-mainmenu_drop').stop().hide();
      $('.b-mainmenu__item').removeClass('opened');
    });
});
